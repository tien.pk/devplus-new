@component('mail::message')
    <h1>{{ $mailData['title'] }}</h1>
    <p>{{ $mailData['message'] }}</p>
    @component('mail::button', ['url' => $mailData['url']])
        {{ $mailData['button'] }}
    @endcomponent
    Thanks,<br>
    {{ config('app.name') }}
@endcomponent
