@extends('admin.layouts.layout')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Danh sách học viên</h3>
                </div>

                <div class="card-body">
                    <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                        <div class="row mb-2">
                            <div class="col-sm-12 col-md-12 d-flex justify-content-end">
                                <a href="{{ route('AddStudent') }}" class="btn btn-success"><i class="fa fa-plus"></i> Thêm học viên</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="datatables" class="table table-bordered table-striped dataTable dtr-inline collapsed"
                                       aria-describedby="example1_info">
                                    <thead>
                                    <tr>
                                        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                            aria-label="Browser: activate to sort column ascending">#
                                        </th>
                                        <th class="sorting sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending"
                                            aria-label="Rendering engine: activate to sort column descending">Họ tên
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                            aria-label="Browser: activate to sort column ascending">Ảnh
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                            aria-label="Browser: activate to sort column ascending">Email
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                            aria-label="Platform(s): activate to sort column ascending">Ngày sinh
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                            aria-label="Engine version: activate to sort column ascending">Khóa
                                        </th>
                                        <th class="sorting text-align-center" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                            aria-label="Engine version: activate to sort column ascending">#
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($students as $key => $student)
                                        <tr class="">
                                            <td class="dtr-control sorting_1" tabindex="0">{{ ++$key }}</td>
                                            <td><a href="{{ route('ShowStudent', ['id' => $student->id]) }}">{{ $student->name }}</a></td>
                                            <td><img src="{{ asset('uploads/'.$student->avatar) }}" alt="" width="120px"></td>
                                            <td>{{ $student->email }}</td>
                                            <td>{{ $student->birthday }}</td>
                                            <td>{{ $student->created_at }}</td>
                                            <td>
                                                <a href="{{ route('EditStudent', ['id' => $student->id]) }}" class="text-primary"><i class="bi bi-pencil-square"></i></a>
                                                <a href="javascript:void(0)" class="border-transparent text-danger btn-delete" data-id="{{ $student->id }}"><i class="bi bi-trash"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th rowspan="1" colspan="1">#</th>
                                        <th rowspan="1" colspan="1">Họ tên</th>
                                        <th rowspan="1" colspan="1">Ảnh</th>
                                        <th rowspan="1" colspan="1">Email</th>
                                        <th rowspan="1" colspan="1">Ngày sinh</th>
                                        <th rowspan="1" colspan="1">Khóa</th>
                                        <th rowspan="1" colspan="2" class="">#</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        $(function (){
            $('.btn-delete').click(function(){
                var student_id = $(this).data('id');

                if(confirm('Bạn xác nhận muốn xóa học viên này') === true) {
                    $.ajax({
                        headers:{
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        type: 'POST',
                        url: '{{ route('DeleteStudent') }}',
                        data: {
                            id: student_id
                        },
                        dataType: "json",
                        success: function (data) {
                            alert(data.message);
                            window.location.reload();
                        }
                    });
                }
            })
        })
    </script>
@endsection

