@extends('admin.layouts.layout')
@section('content')
    <div class="row">
        <div class="col-md-8">
            <div class="card card-primary">
                <form method="post" action="{{ route('ProcEditStudent', ['id' => $student->id]) }}" enctype="multipart/form-data">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label for="exampleInputName">Họ tên</label>
                            <input type="text" name="name" value="{{ $student->name }}" class="form-control" id="exampleInputName"
                                placeholder="Enter name">
                            @error('name')
                            <span class="text text-danger">{{ $message }}</span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Email</label>
                            <input type="email" name="email" value="{{ $student->email }}" class="form-control" id="exampleInputEmail"
                                placeholder="Enter email">
                            @error('email')
                            <span class="text text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="dataOfBirth">Ngày sinh</label>
                            <input type="date" name="birthday" value="{{ $student->birthday }}" class="form-control" id="dataOfBirth">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputFile">Ảnh</label><br>
                            <img src="{{ asset('uploads/'.$student->avatar) }}" alt="" width="150px">

                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" name="avatar" class="custom-file-input" id="exampleInputFile">
                                    <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                </div>
                                <div class="input-group-append">
                                    <span class="input-group-text">Upload</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Cập nhật</button>
                        <a href="{{ route('ListStudent') }}" class="btn btn-outline-dark">Quay lại</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
