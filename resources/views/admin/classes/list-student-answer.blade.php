@extends('admin.layouts.layout')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <h3 class="card-title">Danh sách học viên</h3>
                    <div class="ml-4">
                        {{ 'Đã nộp: ' . $listStudentHandIn . '/' . count($students) }}
                    </div>
                    <div class="ml-4">
                        {{ 'Đã chấm điểm: ' . $listStudentHandInScore . '/' . count($students)}}
                    </div>
                </div>
                <div class="card-body">
                    <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="datatables" class="table table-bordered table-striped dataTable dtr-inline collapsed"
                                       aria-describedby="example1_info">
                                    <thead>
                                    <tr>
                                        <th class="sorting">#</th>
                                        <th class="">Họ tên</th>
                                        <th class="">Bài nộp</th>
                                        <th class="">Hạn</th>
                                        <th class="">Trạng thái</th>
                                        <th class="col-1">Điểm</th>
                                        <th class="col-1">Gia hạn</th>
                                        <th class="col-1">Lý do</th>
                                        <th class="sorting">#</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($students as $key => $student)
                                        <tr>
                                            <td class="dtr-control sorting_1" tabindex="0">{{ ++$key }}</td>
                                            <td>{{ $student->user?->name }}</td>
                                            <td>{{ $student->answer_link}}</td>
                                            <td>{{ \Carbon\Carbon::parse($student->date_line)->format('Y-m-d H:i') }}</td>
                                            <td><span
                                                    class="text {{ $student->status == 1 ? 'text-danger' : 'text-success' }}">{{ $student->status == 1 ? 'Chưa nộp' : 'Đã nộp' }}</span>
                                            </td>
                                            <td><input class="col-12 score{{ $student->id }}" value="{{ $student->score }}" type="number" min="0" max="10"  {{ $student->status == 2 ? '' : 'disabled' }}></td>
                                            <td><input class="col-12 add-time{{ $student->id }}" type="number"></td>
                                            <td><input class="reason{{ $student->id }}" value="{{ $student->reason_late }}" type="text"></td>
                                            <td>
                                                <button data-id="{{ $student->id }}" class="btn btn-success btn-submit"><i class="bi bi-save-fill"></i></button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th rowspan="1" colspan="1">#</th>
                                        <th rowspan="1" colspan="1">Họ tên</th>
                                        <th rowspan="1" colspan="1">Bài nộp</th>
                                        <th rowspan="1" colspan="1">Thời gian nộp</th>
                                        <th rowspan="1" colspan="1">Trạng thái</th>
                                        <th rowspan="1" colspan="1">Điểm</th>
                                        <th rowspan="1" colspan="1">Gia hạn</th>
                                        <th rowspan="1" colspan="1">Lý do</th>
                                        <th rowspan="1" colspan="1">#</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
@section('js')
    <script type="text/javascript">
        $(function(){
            $('.btn-submit').click(function(e){
                e.preventDefault();
                var id = $(this).data('id')
                var score = $('.score'+id).val();
                var add_time = $('.add-time'+id).val();
                var reason = $('.reason'+id).val();

               $.ajax({
                   headers: {
                       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                   },
                   type: "POST",
                   url: "{{ route('SaveScore') }}",
                   data: {
                       id: id,
                       score: score,
                       add_time: add_time,
                       reason: reason
                   },
                   success: function (data) {
                        alert(data.message);
                        // window.location.reload();
                   },
               })
            })
        })
    </script>
@endsection
