@extends('admin.layouts.layout')
@section('content')
    <div class="row">
        <div class="col-md-8">
            <div class="card card-primary">
                <form method="post" action="{{ route('ProcEditClass', $class->id) }}" enctype="multipart/form-data">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label for="exampleInputName">Tên lớp</label>
                            <input type="text" name="title" value="{{ $class->title }}" class="form-control" id="exampleInputName"
                                   placeholder="Enter name">
                            @error('title')
                            <span class="text text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="dataOfBirth">Khóa học</label>
                            <select name="course_id" class="form-control">
                                <option>---</option>
                                @foreach($courses as $course)
                                    <option value="{{ $course->id }}" {{ $course->id == $class->course_id ? 'selected' : '' }}>{{ $course->title }}</option>
                                @endforeach
                            </select>
                            @error('teacher')
                            <span class="text text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="dataOfBirth">Giảng viên</label>
                            <select name="teacher_id" class="form-control">
                                <option>---</option>
                                @foreach($teachers as $teacher)
                                    <option value="{{ $teacher->id }}" {{ $teacher->id == $class->user_id ? 'selected' : '' }}>{{ $teacher->email }}</option>
                                @endforeach
                            </select>
                            @error('teacher')
                            <span class="text text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="exampleInputFile">Sinh viên</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" name="avatar" class="custom-file-input" id="exampleInputFile">
                                    <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                </div>
                                <div class="input-group-append">
                                    <span class="input-group-text">Upload</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Cập nhật</button>
                        <a href="{{ route('ListClass') }}" class="btn btn-outline-dark">Quay lại</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
