@extends('admin.layouts.layout')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Danh sách học viên</h3>
                </div>

                <div class="card-body">
                    <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="datatables" class="table table-bordered table-striped dataTable dtr-inline collapsed"
                                       aria-describedby="example1_info">
                                    <thead>
                                    <tr>
                                        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                            aria-label="Browser: activate to sort column ascending">#
                                        </th>
                                        <th class="sorting sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending"
                                            aria-label="Rendering engine: activate to sort column descending">Họ tên
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                            aria-label="Engine version: activate to sort column ascending">#
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
{{--                                    @foreach($students as $key => $student)--}}
{{--                                        <tr class="">--}}
{{--                                            <td class="dtr-control sorting_1" tabindex="0">{{ ++$key }}</td>--}}
{{--                                            <td><a href="{{ route('ShowStudent', ['id' => $student->id]) }}">{{ $student->name }}</a></td>--}}
{{--                                            <td>--}}
{{--                                                <a href="{{ route('EditStudent', ['id' => $student->id]) }}" class="text-primary"><i class="bi bi-pencil-square"></i></a>--}}
{{--                                            </td>--}}
{{--                                        </tr>--}}
{{--                                    @endforeach--}}
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th rowspan="1" colspan="1">#</th>
                                        <th rowspan="1" colspan="1">Họ tên</th>
                                        <th rowspan="1" colspan="1">#</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
