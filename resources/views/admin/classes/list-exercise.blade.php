@extends('admin.layouts.layout')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Danh sách bài tập</h3>
                </div>

                <div class="card-body">
                    <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                        <div class="row mb-2">
                            <div class="col-sm-12 col-md-12 d-flex justify-content-end">
                                <a href="{{ route('AddExercise', ['id' => $id]) }}" class="btn btn-success"><i class="fa fa-plus"></i> Thêm bài tập</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="datatables" class="table table-bordered table-striped dataTable dtr-inline collapsed"
                                       aria-describedby="example1_info">
                                    <thead>
                                    <tr>
                                        <th class="sorting">#</th>
                                        <th class="sorting">Bài tập</th>
                                        <th class="sorting">Chương</th>
                                        <th class="sorting">Link</th>
                                        <th class="sorting">File</th>
                                        <th class="sorting">Hạn ngày</th>
                                        <th class="sorting">Trạng thái</th>
                                        <th class="sorting" colspan="2">Hành động</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($exercises as $key => $exercise)
                                        <tr class="">
                                            <td class="dtr-control sorting_1" tabindex="0">{{ ++$key }}</td>
                                            <td><a href="{{ route('ListAnswerStudent', [1, $exercise->id]) }}">{{ $exercise->title }}</a></td>
                                            <td>{{ $exercise->chapter_id }}</td>
                                            <td><a href="{{ $exercise->question_link }}">{{ $exercise->question_link }}</a></td>
                                            <td></td>
                                            <td>{{ $exercise->end_time }}</td>
                                            <td></td>
                                            <td>
                                                <a href="{{ route('EditStudent', ['id' => $exercise->id]) }}" class="text-primary"><i
                                                        class="bi bi-pencil-square"></i></a>
                                                <a href="{{ route('Statistic', [1, $exercise->id]) }}" class="text-default"><i
                                                    class="bi bi-activity"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th rowspan="1" colspan="1">#</th>
                                        <th rowspan="1" colspan="1">Bài tập</th>
                                        <th rowspan="1" colspan="1">Chương</th>
                                        <th rowspan="1" colspan="1">Link</th>
                                        <th rowspan="1" colspan="1">File</th>
                                        <th rowspan="1" colspan="1">Hạn ngày</th>
                                        <th rowspan="1" colspan="1">Trạng thái</th>
                                        <th rowspan="1" colspan="1">#</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
