@extends('admin.layouts.layout')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Danh sách lớp</h3>
                </div>

                <div class="card-body">
                    <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                        <div class="row mb-2">
                            <div class="col-sm-12 col-md-12 d-flex justify-content-end">
                                <a href="{{ route('AddClass') }}" class="btn btn-success"><i class="fa fa-plus"></i> Thêm lớp</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="datatables" class="table table-bordered table-striped dataTable dtr-inline collapsed"
                                       aria-describedby="example1_info">
                                    <thead>
                                    <tr>
                                        <th class="sorting">#</th>
                                        <th class="sorting sorting_asc">Lớp</th>
                                        <th class="sorting">Khóa</th>
                                        <th class="sorting">Giảng viên</th>
                                        <th class="sorting">Số lượng</th>
                                        <th class="sorting">#</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($classes as $key => $class)
                                        <tr class="">
                                            <td class="dtr-control sorting_1" tabindex="0">{{ ++$key }}</td>
                                            <td><a href="{{ route('ListExercise', ['id' => $class->id]) }}">{{ $class->title }}</a></td>
                                            <td>{{ $class->course?->title }}</td>
                                            <td>{{ $class->user->name }}</td>
                                            <td>{{ \App\Models\PostModel::where('class_id', $class->id)->get()->count() }}</td>
                                            <td class="d-flex align-items-center">
                                                <a href="{{ route('EditClass', $class->id) }}" class="text-primary"><i
                                                        class="bi bi-pencil-square mr-2"></i></a>
                                                <a href="javascript:void(0)" class="border-transparent text-danger btn-delete" data-id="{{ $class->id }}"><i class="bi bi-trash"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th rowspan="1" colspan="1">#</th>
                                        <th rowspan="1" colspan="1">Lớp</th>
                                        <th rowspan="1" colspan="1">Khóa</th>
                                        <th rowspan="1" colspan="1">Giảng viên</th>
                                        <th rowspan="1" colspan="1">Số lượng</th>
                                        <th rowspan="1" colspan="1">#</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        $(function (){
            $('.btn-delete').click(function(e){
                e.preventDefault();
                var class_id = $(this).data('id');

                if(confirm('Bạn xác nhận muốn xóa lớp này') === true) {
                    $.ajax({
                        headers:{
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        type: 'POST',
                        url: '{{ route('DeleteClass') }}',
                        data: {
                            id: class_id
                        },
                        dataType: "json",
                        success: function (data) {
                            alert(data.message);
                            window.location.reload();
                        }
                    });
                }
            })
        })
    </script>
@endsection
