@extends('admin.layouts.layout')
@section('content')

    <div class="row">
        <div id="donutchart" style="width: 550px; height: 500px;"></div>
        <div id="donutchart-2" style="width: 550px; height: 500px;"></div>
    </div>

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

    <script type="text/javascript">
      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['Tổng (' + {{ $listAnswerHandIn }} + ')',      {{ $listAnswerHandIn }}],
          ['Đã chấm điểm (' + {{ $listStudentHandInScore }} + ')',     {{ $listStudentHandInScore }}]
        ]);
        var options = {
          title: 'Tỉ lệ chấm bài của giảng viên',
          pieHole: 0.4,
        };
        var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
        chart.draw(data, options);
      }
    </script>

    <script type="text/javascript">
        google.charts.load("current", {packages:["corechart"]});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = google.visualization.arrayToDataTable([
                ['Task', 'Hours per Day'],
                ['Đã Nộp (' + {{ $answers }} + ')',      {{ $answers }}],
                ['Chưa Nộp (' + {{ $notAnswers }} + ')',     {{ $notAnswers }}],
                ['Nộp Muộn (' + {{ $lateAnswers }} + ')' ,  {{ $lateAnswers }}]
            ]);
            var options = {
                title: 'Tỉ lệ nộp bài của học viên',
                pieHole: 0.4,
            };
            var chart = new google.visualization.PieChart(document.getElementById('donutchart-2'));
            chart.draw(data, options);
        }
    </script>
@endsection
