@extends('admin.layouts.layout')
@section('content')
    <div class="row">
        <div class="col-md-8">
            <div class="card card-primary">
                <form method="post" action="{{ route('ProcAddCourse') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label for="exampleInputName">Tên khóa học</label>
                            <input type="text" name="title" value="{{ old('name') }}" class="form-control" id="exampleInputName">
                            @error('title')
                            <span class="text text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Thêm</button>
                            <a href="{{ route('ListCourse') }}" class="btn btn-outline-dark">Quay lại</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
