@extends('admin.layouts.layout')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Danh sách khóa học</h3>
                </div>

                <div class="card-body">
                    <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                        <div class="row mb-2">
                            <div class="col-sm-12 col-md-12 d-flex justify-content-end">
                                <a href="{{ route('AddCourse') }}" class="btn btn-success"><i class="fa fa-plus"></i> Thêm khóa học</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="datatables" class="table table-bordered table-striped dataTable dtr-inline collapsed"
                                       aria-describedby="example1_info">
                                    <thead>
                                    <tr>
                                        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                            aria-label="Browser: activate to sort column ascending">#
                                        </th>
                                        <th class="sorting sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending"
                                            aria-label="Rendering engine: activate to sort column descending">Tên khóa học
                                        </th>
                                        <th class="sorting sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending"
                                            aria-label="Rendering engine: activate to sort column descending">Tổng số chương
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                            aria-label="Engine version: activate to sort column ascending">#
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($courses as $key => $course)
                                        <tr class="">
                                            <td class="dtr-control sorting_1" tabindex="0">{{ ++$key }}</td>
                                            <td><a href="{{ route('ListChapter', ['id' => $course->id]) }}">{{ $course->title }}</a></td>
                                            <td>{{ \App\Models\ChapterModel::where('course_id', $course->id)->get()->count() }}</td>
                                            <td>
{{--                                                <a href="{{ route('EditStudent', ['id' => $course->id]) }}" class="text-primary"><i class="bi bi-pencil-square"></i></a>--}}
                                                <a href="javascript:void(0)" class="border-transparent text-danger btn-delete" data-id="{{ $course->id }}"><i class="bi bi-trash"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th rowspan="1" colspan="1">#</th>
                                        <th rowspan="1" colspan="1">Khóa học</th>
                                        <th rowspan="1" colspan="1">Tổng số chương</th>
                                        <th rowspan="1" colspan="1">#</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        $(function (){
            $('.btn-delete').click(function(){
                var course_id = $(this).data('id');

                if(confirm('Bạn xác nhận muốn xóa khóa học này') === true) {
                    $.ajax({
                        headers:{
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        type: 'POST',
                        url: '{{ route('DeleteCourse') }}',
                        data: {
                            id: course_id
                        },
                        dataType: "json",
                        success: function (data) {
                            alert(data.message);
                            window.location.reload();
                        }
                    });
                }
            })
        })
    </script>
@endsection
