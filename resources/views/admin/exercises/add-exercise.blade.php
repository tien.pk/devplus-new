@extends('admin.layouts.layout')
@section('content')
    <div class="row">
        <div class="col-md-8">
            <div class="card card-primary">
                <form method="post" action="{{ route('ProcAddExercise', ['id' => $id]) }}" enctype="multipart/form-data">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label for="exampleInputName">Tên bài tập</label>
                            <input type="text" name="title" value="{{ old('title') }}" class="form-control" id="exampleInputName">
                            @error('title')
                            <span class="text text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="dataOfBirth">Chương</label>
                            <select name="chapter_id" class="form-control">
                                <option>---</option>
                                @foreach($chapters as $chapter)
                                    <option value="{{ $chapter->id }}">{{ $chapter->title }}</option>
                                @endforeach
                            </select>
                            @error('chapter_id')
                            <span class="text text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="exampleInputName">Link bài tập</label>
                            <input type="text" name="link" value="{{ old('link') }}" class="form-control" id="exampleInputName">
                            @error('link')
                            <span class="text text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="exampleInputFile">File bài tập</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" name="file" class="custom-file-input" id="exampleInputFile">
                                    <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                </div>
                                <div class="input-group-append">
                                    <span class="input-group-text">Upload</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputName">Thời gian</label>
                            <input type="number" name="duration" value="{{ old('duration') }}" class="form-control" id="exampleInputName">
                            @error('duration')
                            <span class="text text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="exampleInputName">Hạn đến</label>
                            <input type="datetime-local" name="end_time" value="{{ old('end_time') }}" class="form-control" id="exampleInputName">
                            @error('end_time')
                            <span class="text text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Thêm</button>
                        <a href="#" class="btn btn-outline-dark">Quay lại</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
