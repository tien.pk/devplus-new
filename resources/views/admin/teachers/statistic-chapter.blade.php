@extends('admin.layouts.layout')
@section('content')
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    @if (!empty($chapters))
        <div class="row">
            @php
                $i = 1;
            @endphp
            @foreach ($chapters as $chapter)
                <div id="donutchart{{ $i }}" class="col-md-6" style="margin-bottom: 25px"></div>
                <script type="text/javascript">
                google.charts.load("current", {packages:["corechart"]});
                google.charts.setOnLoadCallback(drawChart);
                function drawChart() {
                    var data = google.visualization.arrayToDataTable([
                    ['Task', 'Hours per Day'],
                    ['Chưa giao (' + {{ $chapter->quantity - $chapter->countPosts }} + ')',      {{ $chapter->quantity - $chapter->countPosts }}],
                    ['Đã giao (' + {{ $chapter->countPosts }} + ')',     {{ $chapter->countPosts }}]
                    ]);
                    var options = {
                    title: 'Tỉ lệ giao bài của giảng viên học phần: {{ $chapter->title }}',
                    pieHole: 0.4,
                    };
                    var chart = new google.visualization.PieChart(document.getElementById('donutchart' + {{ $i }}));
                    chart.draw(data, options);
                }
                </script>
                @php
                $i ++;
                @endphp
            @endforeach
        </div>
    @endif
@endsection
