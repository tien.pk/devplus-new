@extends('admin.layouts.layout')
@section('content')
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    @if (!empty($courses))
        <div class="row">
            @php
                $i = 1;
            @endphp
            @foreach ($courses as $course)
                <a href="{{ route('StatisticTeacherChapter', ['user_id' => $teacher_id, 'course_id' => $course->id]) }}" id="donutchart{{ $i }}" class="col-md-6" style="margin-bottom: 25px"></a>
                <script type="text/javascript">
                google.charts.load("current", {packages:["corechart"]});
                google.charts.setOnLoadCallback(drawChart);
                function drawChart() {
                    var data = google.visualization.arrayToDataTable([
                    ['Task', 'Hours per Day'],
                    ['Chưa giao (' + {{ $course->quantity - $course->countPosts }} + ')',      {{ $course->quantity - $course->countPosts }}],
                    ['Đã giao (' + {{ $course->countPosts }} + ')',     {{ $course->countPosts }}]
                    ]);
                    var options = {
                    title: 'Tỉ lệ giao bài của giảng viên khóa: {{ $course->title }}',
                    pieHole: 0.4,
                    };
                    var chart = new google.visualization.PieChart(document.getElementById('donutchart' + {{ $i }}));
                    chart.draw(data, options);
                }
                </script>
                @php
                $i ++;
                @endphp
            @endforeach
        </div>
    @endif
@endsection
