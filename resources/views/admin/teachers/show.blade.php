@extends('admin.layouts.layout')
@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">{{ $title }}</h3>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                    <i class="fas fa-minus"></i>
                </button>
                <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                    <i class="fas fa-times"></i>
                </button>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-12 col-md-12 col-lg-8 order-2 order-md-1">
                    <div class="row">
                        <div class="col-12 col-sm-4">
                            <a href="{{ route('StatisticTeacher', ['id' => $teachers->id]) }}" class="info-box bg-light">
                                <div class="info-box-content">
                                    <span class="info-box-text text-center text-muted">Thống kê</span>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <table class="table table-hover">
                                <tr>
                                    <th>Họ tên</th>
                                    <td>{{ $teachers->name }}</td>
                                </tr>
                                <tr>
                                    <th>Email</th>
                                    <td>{{ $teachers->email }}</td>
                                </tr>
                                <tr>
                                    <th>Ngày sinh</th>
                                    <td>{{ $teachers->birthday }}</td>
                                </tr>
                                <tr>
                                    <th>Ngày bắt đầu làm việc</th>
                                    <td>{{ $teachers->created_at }}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-12 col-lg-4 order-1 order-md-2">
                    <h3 class="text-primary"><i class="fas fa-phone"></i> Liên lạc</h3>
                    <br>
                    <ul class="list-unstyled">
                        <li>
                            <img class="" src="{{ asset('uploads/'.$teachers->avatar) }}" alt="user image" width="200px">
                        </li>
                        <li>
                            <a href="" class="btn-link text-secondary"><i class="fas fa-envelope"></i> {{ $teachers->email }}</a>
                        </li>
                        {{--                                                <li>--}}
                        {{--                                                    <a href="" class="btn-link text-secondary"><i class="far fa-fw fa-file-pdf"></i> UAT.pdf</a>--}}
                        {{--                                                </li>--}}
                        {{--                        <li>--}}
                        {{--                            <a href="" class="btn-link text-secondary"><i class="far fa-fw fa-envelope"></i> Email-from-flatbal.mln</a>--}}
                        {{--                        </li>--}}
                        {{--                        <li>--}}
                        {{--                            <a href="" class="btn-link text-secondary"><i class="far fa-fw fa-image "></i> Logo.png</a>--}}
                        {{--                        </li>--}}
                        {{--                        <li>--}}
                        {{--                            <a href="" class="btn-link text-secondary"><i class="far fa-fw fa-file-word"></i> Contract-10_12_2014.docx</a>--}}
                        {{--                        </li>--}}
                    </ul>
                    <div class="text-center mt-5 mb-3">
                        <a href="#" class="btn btn-sm btn-primary">Add files</a>
                        <a href="#" class="btn btn-sm btn-warning">Report contact</a>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
