@extends('home.layouts.layout')
@section('content')
<div class="d-flex justify-content-between">
    <p class="title">DASHBOARD</p>
    <i class="fa-sharp fa-solid fa-magnifying-glass"></i>
</div>
<form class="form-row justify-content-between">
    <div class="row search-row">
        <div class="form-group col-md-1.5">
            <label for="class">Tên lớp</label>
            <input type="search" name="class" id="class" class="form-control">
        </div>
        <div class="form-group col-md-1.5">
            <label for="teacher">Tên giáo viên</label>
            <input type="search" name="teacher" id="class" class="form-control">
        </div>
        <div class="form-group col-md-1.5">
            <label for="">Ngày tạo lớp</label>
            <input type="date" name="date" id="class" class="form-control">
        </div>
    </div>
    <div class="form-group">
        <button type=" submit " class="btnSubmit btn btn-primary ">Tìm kiếm</button>
    </div>
</form>
<form class="form-row-right">
    <div class="form-row justify-content-between">
        <div class="col">
            <label for="">Sắp xếp</label>
            <select name="" id="sortByName">
                <option value="" disabled selected>Chọn 1 mục</option>
                <option value="">Khóa học</option>
                <option value="">Tên lớp</option>
                <option value="">Giáo viên </option>
                <option value="">Ngày đăng bài</option>
            </select>
            <select name="" id="sortByTime">
                <option value="">Tăng dần</option>
                <option value="">Giảm dần</option>

            </select>

        </div>
        <div class="col-3.5">
            <label for="">Xem</label>
            <select name="" id="sortByQuantity">
                <option value="">15</option>
                <option value="">20</option>
                <option value="">25</option>
                <option value="">30</option>
            </select>
            <label for="">mục</label>
        </div>
    </div>
</form>
<!-- Main-content -->
<div class="main-content">
    <table class="table align-items-center">
        <thead>
            <thead>
                <tr>
                    <th scope="col">Khóa học</th>
                    <th scope="col">Tên lớp</th>
                    <th scope="col">Giáo viên</th>
                    <th scope="col">Ngày tạo lớp</th>
                </tr>
            </thead>
        <tbody id="tbody">
            @foreach ($listClass as $list)
                <tr class="example1 align-items-center">
                    <td><a href="{{ route('ManageExercise', ['id' => $list->id]) }}">{{ $list->class_title }}</a></td>
                    <td>{{ $list->course_title }}</td>
                    <td>{{ $list->user_title }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
{{--    <nav aria-label="...">--}}
{{--        <ul class="pagination pagination-sm justify-content-center">--}}
{{--            {!! $listClass->links() !!}--}}
{{--        </ul>--}}
{{--    </nav>--}}
</div>
@endsection()
