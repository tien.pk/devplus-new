@extends('home.layouts.layout')
@section('content')
    <div class="d-flex justify-content-between">
        <h2 class="title">PHP - WEB1202 </h2>
        <i class="fa-sharp fa-solid fa-magnifying-glass"></i>
    </div>
    <!-- Main-content -->
    <div style="padding-top:50px" class="content d-flex justify-content-between">
        <div class="right-content col-md-8">
            @if ( Session::has('success') )
                <div class="alert alert-success d-flex align-items-center" role="alert">
                    <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Success:"><use xlink:href="#check-circle-fill"/></svg>
                    <div>
                        {{ Session::get('success') }}
                    </div>
                </div>
            @endif
            @if ( Session::has('error') )
                <div class="alert alert-danger d-flex align-items-center" role="alert">
                    <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>
                    <div>
                        {{ Session::get('error') }}
                    </div>
                </div>
            @endif
            <div class="post">
                <h4 style="font-size: 20px; color: #000">{{ $post->title }}</h4>
                <p>Link bài tập: <a href="{{ $post->question_link }}">Tại đây</a></p>
            </div>
            <div class="comment">
                <hr>
                @if (!empty($comments))
                    @foreach ($comments as $comment)
                        <p class="text-primary">{{ $comment->user_name }}</p> <span></span>
                        <p>{{ $comment->title }}</p>
                    @endforeach
                @else
                    <span style="font-size:18px; color:#ccc">Hãy là người bình luận đầu tiên !</span>
                @endif
            </div>
            <br>
            <form action="{{ route('SaveComment') }}" method="POST">
                @csrf
                @error('title')
                    <span style="font-size: 18px; color: red">{{ $message }}</span>
                @enderror
                <div class='form-group' id="content">
                    <span id='error' class='text-danger'></span>
                    <input type="hidden" name="post_id" value="{{ $post->id }}">
                    <input type='text' name='title' class="form-control" placeholder="Nhập nội dung bình luận" />
                    <button type='submit' class='btn btn-default' id="submit"><svg focusable="false" width="24"
                            height="24" viewBox="0 0 24 24" class=" NMm5M hhikbc">
                            <path d="M2 3v18l20-9L2 3zm2 11l9-2-9-2V6.09L17.13 12 4 17.91V14z"></path>
                        </svg></button>
                </div>
            </form>

        </div>
        <div class="left-content">
            <div class="d-flex justify-content-around align-items-center">
                <p>Bài làm</p>
                <p>{{ !empty($answer->answer_link) ? (!empty($answer->add_time) ? 'Nộp muộn' : 'Đã nộp') : 'Chưa nộp'}}</p>
            </div>
            <div class="d-flex justify-content-around align-items-center">
                <div id="hand-in" style="display:{{ !empty($answer->answer_link) ? 'block' : 'none'}}">
                    <p>File đã nộp: <a href="{{ asset('answers/'.$answer->answer_link) }}" download="{{ $answer->answer_link }}">Tải xuống để xem</a></p>
                    <button style="float:right" class="btn btn-warning" onclick="edit()">Sửa</button>
                </div>
                <form action="{{ route('SaveHandIn') }}" method="POST" id="form-hand-in" enctype="multipart/form-data" style="display:{{ !empty($answer->answer_link) ? 'none' : 'block'}}">
                    @csrf
                    <div class="form-group">
                        <input type="hidden" name="post_id" value="{{ $post->id }}">
                        @error('file')
                            <span style="font-size: 18px; color: red">{{ $message }}</span>
                        @enderror
                        <input type="file" name="file" id="file" class="form-control">
                    </div>
                    <div class="form-group">
                        <button style="float:right" class="btn btn-primary">Nộp bài</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <style>
        #submit {
            position: absolute;
            top: 0px;
            right: 0px;
        }

        #content {
            position: relative;
        }

        p {
            font-size: 18px;
            color: #000;
        }
    </style>
    <script>
        function edit(){
            $('#form-hand-in').css('display', 'block');
            $('#hand-in').css('display', 'none');
        }
    </script>
@endsection()
