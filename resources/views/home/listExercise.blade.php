@extends('home.layouts.layout')
@section('content')
    <div class="d-flex justify-content-between">
        <p class="title">PHP - WEB1202 </p>
        <i class="fa-sharp fa-solid fa-magnifying-glass"></i>
    </div>
    <form class="form-row justify-content-between">
        <div class="row search-row">
            <div class="form-group col-md-1.5">
                <label for="class">Tên chương </label>
                <input type="search" name="class" id="class" class="form-control">
            </div>
            <div class="form-group col-md-1.5">
                <label for="">Ngày giao bài tập</label>
                <input type="date" name="date" id="class" class="form-control">
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btnSubmit btn btn-primary ">Tìm kiếm</button>
        </div>
    </form>
    <form class="form-row-right">
        <div class="form-row justify-content-between">
            <div class="col">
                <label for="">Sắp xếp</label>
                <select name="" id="sortByName">
                    <option value="" disabled selected>Chọn 1 mục</option>
                    <option value="">Trạng thái</option>
                    <option value="">Tiêu đề</option>
                    <option value="">Chương</option>
                    <option value="">Ngày giao bài tập</option>
                </select>
                <select name="sortByIsWork" id="sortByIsWork">
                    <option value="4" selected>Mặc định</option>
                    @if (count($listExcercise)>0)
                        <option value="0">Chưa hoàn thành</option>
                        <option value="1">Đã hoàn thành</option>
                        <option value="2">Quá hạn</option>
                    @endif
                </select>

            </div>
            <div class="col-3.5">
                <label for="">Xem</label>
                <select name="" id="sortByQuantity">
                    <option value="">15</option>
                    <option value="">20</option>
                    <option value="">25</option>
                    <option value="">30</option>
                </select>
                <label for="">mục</label>
            </div>
        </div>
    </form>
    <!-- Main-content -->
    <div class="main-content">
        <table class="table align-items-center">
            <thead>
            <tr>
                <th scope="col">Tiêu đề</th>
                <th scope="col">Chương</th>
                <th scope="col">Ngày đăng bài</th>
                <th scope="col" colspan="2">Trạng thái</th>
            </tr>
            </thead>
            <tbody id="tbody">
            @foreach ($listExcercise as $list)
                <tr class="example1 align-items-center">

                    <td><a href="{{ route('ExerciseHandIn', ['id' => $list->id]) }}">{{ $list->title }}</a></td>
                    <td>{{ $list->chapter_title }}</td>
                    <td>{{ date('d-m-Y ', strtotime($list->start_time)) }}</td>
                    @if ($list->is_work == 2)
                        <td class="course">
                            <p class="btn btn-secondary">Quá hạn</p>
                        </td>
                    @elseif ($list->is_work == 0)
                        <td class="course">
                            <p class="btn btn-danger">Chưa hoàn thành</p>
                        </td>
                    @elseif ($list->is_work == 1)
                        <td class="course">
                            <p class="btn btn-success">Đã hoàn thành</p>
                        </td>
                    @endif
                </tr>
            @endforeach
            </tbody>
        </table>
        <nav aria-label="...">
            <ul class="pagination pagination-sm justify-content-center">
{{--               {!! $listExcercise->links !!}--}}
            </ul>
        </nav>
    </div>
    <!-- /Main-content -->

@endsection
@section('js')
    <script>
        $(document).ready(function () {
            $("#sortByIsWork").on('change', function () {
                var filter = $(this).val();
                // console.log(filter);
                $.ajax({
                    url: "{{ route('ManageExercise', ['id' => $class_id]) }}",
                    type: "GET",
                    data: {listExcercise: filter},
                    success: function (data) {
                        var listExcercise = data.listExcercise;
                        var html = '';
                        if (listExcercise.length > 0) {
                            var date = new Date()
                            for (let i = 0; i < listExcercise.length; i++) {
                                var id = listExcercise[i]['id'];
                                html += '<tr class="example1 align-items-center">' +
                                    '<td style="padding-top: 20px;"><a href="{{ route("ExerciseHandIn", ["id" => ' + id + ']) }}">' + listExcercise[i]['title'] + '</a></td>' +
                                    '<td style="padding-top: 20px;">' + listExcercise[i]['chapter_title'] + '</td>' +
                                    '<td style="padding-top: 20px;">' + listExcercise[i]['start_time'] + '</td>';
                                if (listExcercise[i]['is_work'] == 2) {
                                    html += '<td class="course">' +
                                        '<a href="" class="btn btn-secondary">Quá hạn</a>' +
                                        '</td>';
                                } else if (listExcercise[i]['is_work'] == 0) {
                                    html += '<td class="course">' +
                                        '<a href="" class="btn btn-danger">Chưa hoàn thành</a>' +
                                        '</td>';
                                } else if (listExcercise[i]['is_work'] == 1) {
                                    html += '<td class="course">' +
                                        '<a href="" class="btn btn-primary">Đã hoàn thành</a>' +
                                        '</td>';
                                }
                                html += '</tr>';
                            }
                        }
                        $("#tbody").html(html);
                        console.log(listExcercise.length);
                    }
                });
            });
        });
    </script>
@endsection
