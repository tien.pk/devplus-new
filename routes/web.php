<?php

use App\Http\Controllers\Auth\LoginController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

// Admin
use App\Http\Controllers\Admins\ChapterController;
use App\Http\Controllers\Admins\ClassController;
use App\Http\Controllers\Admins\CourseController;
use App\Http\Controllers\Admins\HomeController as AdminController;
use App\Http\Controllers\Admins\StudentController;
use App\Http\Controllers\Admins\TeacherController;
use App\Http\Controllers\Admins\ExerciseController as ExerciseAdminController;

// Frontend
use App\Http\Controllers\Homes\HomeController;
use App\Http\Controllers\Homes\ExerciseController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::middleware('auth')->group(function () {
    Route::post('logout', [LoginController::class, 'logout'])->name('logout');

// Frontend
    Route::middleware(['role:student'])->group(function () {
        Route::get('/', [HomeController::class, 'index'])->name('Home');
        Route::get('list-exercise/{id}', [ExerciseController::class, 'index'])->name('ManageExercise');
        Route::get('exercise-hand-in', [ExerciseController::class, 'exerciseHandIn'])->name('ExerciseHandIn');
        Route::get('exercise-hand-in/{id}', [ExerciseController::class, 'exerciseHandIn'])->name('ExerciseHandIn');
        Route::post('save-hand-in', [ExerciseController::class, 'saveHandIn'])->name('SaveHandIn');
        Route::post('save-comment', [ExerciseController::class, 'saveComment'])->name('SaveComment');
    });

// Admin
    Route::group(['prefix' => 'admin', 'middleware' => ['role:admin|teacher']], function () {
        Route::get('/', [AdminController::class, 'index'])->name('Dashboard');

        Route::middleware('role:admin')->group(function () {

            // Student
            Route::group(['prefix' => 'students'], function () {
                Route::controller(StudentController::class)->group(function () {
                    Route::get('', 'listStudent')->name('ListStudent');
                    Route::get('add', 'showAddStudent')->name('AddStudent');
                    Route::post('proc-add', 'procAddStudent')->name('ProcAddStudent');
                    Route::get('show/{id}', 'showStudent')->name('ShowStudent');
                    Route::get('edit/{id}', 'showEditStudent')->name('EditStudent');
                    Route::post('update/{id}', 'procEditStudent')->name('ProcEditStudent');
                    Route::post('delete-student', 'deleteStudent')->name('DeleteStudent');
                    Route::post('import-excel', 'importExcel')->name('ImportExcel');
                });
            });

            // Teacher
            Route::group(['prefix' => 'teachers'], function () {
                Route::controller(TeacherController::class)->group(function () {
                    Route::get('', 'listTeacher')->name('ListTeacher');
                    Route::get('add', 'showAddTeacher')->name('AddTeacher');
                    Route::post('proc-add', 'procAddTeacher')->name('ProcAddTeacher');
                    Route::get('show/{id}', 'showTeacher')->name('ShowTeacher');
                    Route::get('edit/{id}', 'showEditTeacher')->name('EditTeacher');
                    Route::post('update/{id}', 'procEditTeacher')->name('ProcEditTeacher');
                    Route::post('delete-teacher', 'deleteTeacher')->name('DeleteTeacher');
                    Route::get('statistic/{id}', 'statisticTeacher')->name('StatisticTeacher');
                    Route::get('statistic/chapter/{user_id}/{course_id}', 'statisticTeacherChapter')->name('StatisticTeacherChapter');
                });
            });

            // Course
            Route::group(['prefix' => 'courses'], function () {
                Route::controller(CourseController::class)->group(function () {
                    Route::get('', 'listCourse')->name('ListCourse');
                    Route::get('add', 'showAddCourse')->name('AddCourse');
                    Route::post('proc-add', 'procAddCourse')->name('ProcAddCourse');
                    Route::post('delete-course', 'deleteCourse')->name('DeleteCourse');
                });
            });

            // Chapter
            Route::group(['prefix' => 'chapters', 'controller' => ChapterController::class], function () {
                Route::get('/{id}', 'listChapter')->name('ListChapter');
                Route::get('{id}/add', 'showAddChapter')->name('AddChapter');
                Route::post('{id}/proc-add', 'procAddChapter')->name('ProcAddChapter');
                Route::post('delete-chapter', 'deleteChapter')->name('DeleteChapter');
            });

        });

        Route::group(['prefix' => 'classes'], function () {
            Route::controller(ClassController::class)->group(function () {
                Route::get('', 'listClass')->name('ListClass');
                Route::get('add', 'showAddClass')->name('AddClass');
                Route::post('proc-add', 'procAddClass')->name('ProcAddClass');
                Route::post('delete-class', 'deleteClass')->name('DeleteClass');
                Route::get('edit/{id}', 'showEditClass')->name('EditClass');
                Route::post('proc-edit/{id}', 'procEditClass')->name('ProcEditClass');
                Route::get('{id}/list-student', 'listStudent')->name('ListStudentClass');
                Route::get('{id}/statistic/{exid}', 'statistic')->name('Statistic');
            });

            Route::controller(ExerciseAdminController::class)->group(function () {
                Route::get('{id}/list-exercise', 'listExercise')->name('ListExercise');
                Route::get('{id}/list-exercise/add', 'showAddExercise')->name('AddExercise');
                Route::post('{id}/list-exercise/add', 'procAddExercise')->name('ProcAddExercise');
                Route::get('{id}/list-exercise/{exid}', 'listAnswerStudent')->name('ListAnswerStudent');
            });
        });


    });
});
