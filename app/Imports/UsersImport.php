<?php

namespace App\Imports;

use App\Models\UserModel;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;

class UsersImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new UserModel([
            'name'     => $row[1],
            'email'    => $row[2],
            'password' => Hash::make($row[4]),
        ]);
    }
}
