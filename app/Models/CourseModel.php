<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CourseModel extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'course';

    protected $fillable = [
        'id',
        'title',
        'start_time',
        'end_time',
        'status',
        'created_at',
        'updated_at'
    ];

    public function teacher() {
//        return $this->belongsTo();
    }
}
