<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class CommentModel extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'comment';

    protected $fillable = [
        'id',
        'user_id',
        'post_id',
        'title',
        'status',
        'created_at',
        'updated_at'
    ];
    public function loadListWithPager() {
        $query = DB::table($this->table)
        ->select($this->fillable);
        $lists = $query->paginate(10);
        $userModel = new UserModel();
        foreach ($lists as $value){
            $value->user_name = $userModel->loadOne($value->user_id)->name;
        }
        return $lists;
    }

    public function saveNew($data){
        $res = DB::table($this->table)->insertGetId($data);
        return $res;
    }

    public function loadOne($id) {
        $query = DB::table($this->table)
        ->where('id', '=', $id);
        return $query->first();
    }

    public function saveUpdate($data){
        if(empty($data['id'])){
            Session::flash("error", "Không xác định bản ghi cập nhật");
            return null;
        }
        $dataUpdate = [];
        foreach ($data as $colName => $val) {
            if($colName == 'id') continue;
            if(in_array($colName, $this->fillable)){
                $dataUpdate[$colName] = (strlen($val) == 0) ? null : $val;
            }
        }
        $res = DB::table($this->table)
            ->where('id', $data['id'])
            ->update($dataUpdate);
        return $res;
    }
}
