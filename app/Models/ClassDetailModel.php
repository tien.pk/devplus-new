<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use PhpParser\Builder\Class_;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClassDetailModel extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'class_detail';

    protected $fillable = [
        'id',
        'class_id',
        'user_id',
        'status',
        'created_at',
        'updated_at'
    ];

    public function user(){
        return $this->belongsTo(UserModel::class, 'user_id', 'id');
    }

    public function classModel(){
        return $this->belongsTo(ClassModel::class, 'class_id','id');
    }

    public function teacherName(){
        $classModel = ClassModel::where('id',$this->class_id)->first();
        $teacherName = User::where('id',$classModel->user_id);
        return $teacherName->first()->name;
    }

    // public function course(){
    //     $classModel = ClassModel::where('id',$this->class_id)->first();
    // }
}
