<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClassModel extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'class';

    protected $fillable = [
        'id',
        'user_id',
        'course_id',
        'title',
        'quantity',
        'status',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function user() {
        return $this->belongsTo(UserModel::class, 'user_id', 'id');
    }

    public function course(){
        return $this->belongsTo(CourseModel::class, 'course_id', 'id');
    }
}
