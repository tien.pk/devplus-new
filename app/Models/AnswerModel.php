<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class AnswerModel extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'answer';

    protected $fillable = [
        'id',
        'user_id',
        'post_id',
        'answer_link',
        'score',
        'evaluate',
        'add_time',
        'reason_late',
        'status',
        'created_at',
        'updated_at'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(UserModel::class, 'user_id', 'id');
    }

    public function post(){
        return $this->belongsTo(PostModel::class, 'post_id', 'id');
    }

    public function loadOne($id) {
        $query = DB::table($this->table)
        ->where('id', '=', $id);
        return $query->first();
    }
    public function saveUpdate($data){
        if(empty($data['user_id']) &&  empty($data['post_id'])){
            Session::flash("error", "Không xác định bản ghi cập nhật");
            return null;
        }
        $dataUpdate = [];
        foreach ($data as $colName => $val) {
            if($colName == 'user_id' && $colName == 'post_id') continue;
            if(in_array($colName, $this->fillable)){
                $dataUpdate[$colName] = (strlen($val) == 0) ? null : $val;
            }
        }
        $res = DB::table($this->table)
            ->where('user_id', $data['user_id'])
            ->where('post_id', $data['post_id'])
            ->update($dataUpdate);
        return $res;
    }
}
