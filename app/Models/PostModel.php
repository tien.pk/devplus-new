<?php

namespace App\Models;

use App\Http\Controllers\Admins\CourseController;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\SoftDeletes;

class PostModel extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'post';

    protected $fillable = [
        'id',
        'class_id',
        'user_id',
        'course_id',
        'chapter_id',
        'title',
        'question_link',
        'is_work',
        'duration',
        'start_time',
        'end_time',
        'created_by',
        'updated_by',
        'status',
        'created_at',
        'updated_at'
    ];

    public function answerUser(){
        $user_id = Auth::id();
        $answer = AnswerModel::where('post_id',"=",$this->id)
                        ->where('user_id',"=",$user_id)
                        ->first();
        return $answer;
    }

    public function course(){
        return $this->belongsTo(CourseModel::class, 'course_id', 'id');
    }

    public function classModel(){
        return $this->belongsTo(ClassModel::class, 'class_id', 'id');
    }
}
