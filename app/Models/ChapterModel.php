<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ChapterModel extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'chapter';

    protected $fillable = [
        'id',
        'course_id',
        'title',
        'quantity',
        'created_at',
        'updated_at'
    ];
}
