<?php

namespace App\Http\Controllers\Homes;

use App\Http\Controllers\Controller;
use App\Models\ClassDetailModel;
use App\Models\ClassModel;
use App\Models\CourseModel;
use App\Models\UserModel;
use App\Util\BreadcrumbUtil;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public $breadcrumb1;
    public $breadcrumb2;

    public function __construct(){
        $this->breadcrumb1 =new BreadcrumbUtil('Home');
//        $this->middleware('auth');
    }

    public function index(){
        $user = Auth::user();
        $classDetails = ClassDetailModel::where("user_id",$user->id)->paginate(15);

        $breadcrumbs = [
            $this->breadcrumb1
        ];

        $listClassDetail = ClassDetailModel::where('user_id', Auth::user()->id)->get();

        foreach ($listClassDetail as $value){
            $class = ClassModel::find($value->class_id);

            if(!empty($class)){
                $value->class_title = $class->title;
                $value->user_title = UserModel::find($class->user_id)->name;
                $value->course_title = CourseModel::find($class->course_id)->title;
            }
        }

        $data_view = [
            'title' => 'Home',
            'breadcrumbs' => $breadcrumbs,
            'listClass' => $listClassDetail
        ];
        return view('home.dashboard', $data_view);
    }

}
