<?php

namespace App\Http\Controllers\Homes;

use App\Http\Controllers\Controller;
use App\Models\AnswerModel;
use App\Models\CommentModel;
use App\Models\PostModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules\File;

class ExerciseController extends Controller
{
    public function index(Request $request, int $id) {
        $listExcercise = PostModel::join('chapter','post.chapter_id','=','chapter.id')->select('post.*','chapter.title as chapter_title')->where('class_id', $id)->where('user_id', Auth::user()->id)->get();
        $filterId = $request->get('listExcercise','');
        if($request->ajax()){
            if(intval($filterId) === 4){
                $listExcercise = PostModel::join('chapter','post.chapter_id','=','chapter.id')
                                            ->select('post.*','chapter.title as chapter_title')
                                            ->where('class_id', $id)
                                            ->where('user_id', Auth::user()->id)
                                            ->get();
                return response()->json(['listExcercise' => $listExcercise]);
            }else{
                $listExcercise = PostModel::join('chapter','post.chapter_id','=','chapter.id')
                                            ->select('post.*','chapter.title as chapter_title')
                                            ->where('class_id', $id)
                                            ->where('user_id', Auth::user()->id)
                                            ->where('is_work',$filterId)
                                            ->get();
                return response()->json(['listExcercise' => $listExcercise]);

            }
        }

        $data_views = [
            'title' => 'List Excercise',
            'listExcercise' => $listExcercise,
            'class_id' => $id
        ];

        return view('home.listExercise', $data_views);
    }

    public function exerciseHandIn(int $id)
    {
        $data_views = [
            'title' => 'Excercise Hand In',
            'comments' => CommentModel::where('post_id',$id),
            'post' => PostModel::find($id),
            'answer' => AnswerModel::where('post_id', $id)->where('user_id', Auth::user()->id)->first()
        ];
        return view('home.newsFeed', $data_views);

    }

    public function saveHandIn(Request $request)
    {
        $method_route = 'ExerciseHandIn';
        Validator::validate($request->all(), [
            'file' => [
                'required',
                File::types(['tiff','web','png','jpeg','jpg','docx','rar','zip','pdf'])
                ->max(4 * 1024),
                ],
            ]);
        $answer_link = $request->file('file');
        $fileName = time() . '-' . $answer_link->getClientOriginalName();
        $pathUpload = public_path() . DIRECTORY_SEPARATOR . 'answers';
        $answerModel = new AnswerModel();
        $data = [];
        $data['answer_link'] = $fileName;
        $data['post_id'] = $request->input('post_id');
        $data['user_id'] = Auth::user()->id;
        $data['status'] = 2;

        $res = $answerModel->saveUpdate($data);
        if ($res === null) {
            return redirect()->route($method_route, ['id' => $request->input('post_id')]);
        } else if ($res > 0) {
            $answer_link->move($pathUpload, $fileName);
            Session::flash('success', "Nộp bài thành công !");
            return redirect()->route($method_route, ['id' => $request->input('post_id')]);
        } else {
            Session::flash('error', "Lỗi nộp bài không thành công !");
            return redirect()->route($method_route, ['id' => $request->input('post_id')]);
        }
    }

    public function saveComment(Request $request)
    {
        $method_route = 'ExerciseHandIn';
        $rules = [
            'title' => 'required|max:255'
        ];
        $messages = [
            'title.required' => 'Không được để trống bình luận',
            'title.max' => 'Bình luận tối đa 255 kí tự',
        ];
        Validator::validate($request->all(), $rules, $messages);
        $data = $request->post();
        $data['user_id'] = Auth::user()->id;
        unset($data['_token']);
        $modelsComment = new CommentModel();
        $res = $modelsComment->saveNew($data);
        if ($res === null) {
            return redirect()->route($method_route, ['id' => $request->input('post_id')]);
        } else if ($res > 0) {
            Session::flash('success', "Bình luận thành công !");
            return redirect()->route($method_route, ['id' => $request->input('post_id')]);
        } else {
            Session::flash('error', "Lỗi bình luận không thành công !");
            return redirect()->route($method_route, ['id' => $request->input('post_id')]);
        }
    }
}
