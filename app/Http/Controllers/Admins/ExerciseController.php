<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use App\Jobs\SendEmail;
use App\Models\AnswerModel;
use App\Models\ChapterModel;
use App\Models\ClassDetailModel;
use App\Models\ClassModel;
use App\Models\PostModel;
use App\Models\UserModel;
use App\Util\BreadcrumbUtil;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ExerciseController extends Controller
{
    public $breadcrumb1;
    public $breadcrumb2;

    public function __construct(Request $request)
    {
        $this->breadcrumb1 = new BreadcrumbUtil('Dashboard', 'Dashboard');
        $this->breadcrumb2 = new BreadcrumbUtil('Exercise');
    }

    /**
     * @param Request $request
     * @param int $class_id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function listExercise(Request $request, int $class_id)
    {
        $breadcrumbs = [
            $this->breadcrumb1,
            $this->breadcrumb2,
            new BreadcrumbUtil('List exercise')
        ];

        $exercises = PostModel::where('class_id', $class_id)->get();

        $dataView = [
            'breadcrumbs' => $breadcrumbs,
            'exercises' => $exercises,
            'id' => $class_id,
            'title' => 'Danh sách bài tập'
        ];

        return view('admin.classes.list-exercise', $dataView);
    }

    /**
     * @param Request $request
     * @param int $class_id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function showAddExercise(Request $request, int $class_id)
    {
        $class = ClassModel::find($class_id);
        $chapters = ChapterModel::where('course_id', $class->course_id)->get();
        $back = route('ListExercise', $class_id);

        $breadcrumbs = [
            $this->breadcrumb1,
            $this->breadcrumb2,
            new BreadcrumbUtil($class->title),
            new BreadcrumbUtil('Add')
        ];

        $dataView = [
            'breadcrumbs' => $breadcrumbs,
            'chapters' => $chapters,
            'title' => 'Thêm bài tập',
            'back' => $back,
            'id' => $class_id
        ];

        return view('admin.exercises.add-exercise', $dataView);
    }

    public function saveScore(Request $request) {
        $validate = Validator::make($request->all(), [
            'id' => 'required'
        ], [
            'id.required' => 'User_id chưa nhập'
        ]);

        if($validate->fails()) {
            return response()->json(['code' => 0, 'message' => $validate->errors()]);
        }

        $id = $request->get('id');
        $score = $request->get('score', null);
        $add_time = $request->get('add_time', null);
        $reason = $request->get('reason', null);

        $answer = AnswerModel::find($id);
        if($answer == null) {
            return response()->json(['code' => 0, 'message' => 'Học viên này không tồn tại']);
        }

        if(!empty($score) && intval($score) > 0) {
            $answer->score = intval($score);
        }

        $answer->reason_late = $reason;
        $answer->date_line = Carbon::parse($answer->date_line)->addMinutes(intval($add_time));

        $flag = $answer->save();
        if($flag) {
            return response()->json(['code' => 1, 'message' => 'Cập nhật thành công']);
        }

        return response()->json(['code' => 0, 'message' => 'Cập nhật không thành công']);
    }

    /**
     * @param Request $request
     * @param $class_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function procAddExercise(Request $request, $class_id)
    {
        Validator::make($request->all(), [
            'title' => 'required'
        ], [
            'title.required' => 'Tên bài tập bắt buộc phải nhập'
        ]);

        $title = $request->get('title', '');
        $chapter_id = $request->get('chapter_id', 0);
        $link = $request->get('link', '');
        $duration = $request->get('duration', 0);
        $end_time = $request->get('end_time', null);

        $exercise = new PostModel();
        $exercise->class_id = $class_id;
        $exercise->user_id = 1;
        $exercise->title = $title;
        $exercise->course_id = 1;
        $exercise->chapter_id = $chapter_id;

        if (!empty($link)) {
            $exercise->question_link = $link;
        }

        if ($request->hasFile('file')) {
            $file = $request->file('file');

            if ($file->isValid()) {
                $fileName = $file->getClientOriginalName();
                $pathUpload = public_path() . DIRECTORY_SEPARATOR . 'uploads';
                $file->move($pathUpload, $fileName);
                $exercise->file = $fileName;
            }
        }

        $exercise->duration = $duration;
        $exercise->start_time = date('Y-m-d H:i:s');
        $exercise->end_time = $end_time;
        $exercise->created_by = 1;

        $flag = $exercise->save();

        if ($flag) {
            $student_ids = ClassDetailModel::where('class_id', $class_id)->get('user_id');

            foreach ($student_ids as $student_id) {
                $answer = new AnswerModel();

                $answer->user_id = $student_id->user_id;
                $answer->post_id = $exercise->id;
                $answer->class_id = $exercise->class_id;
                $answer->date_line = $exercise->end_time;
                $flag_answer = $answer->save();

                if ($flag_answer) {
                    $user = UserModel::find($answer->user_id);

                    $data = [
                        'title' => 'Trung tâm lập trình Devplus',
                        'message' => 'Đã có một bài tập mới được đăng lên bởi ' . ClassDetailModel::find($class_id)->classModel->user->name,
                        'url' => route('ExerciseHandIn', $exercise->id),
                        'button' => 'Nộp bài'
                    ];

                    $emailJob = (new SendEmail($data, $user->email))->delay(Carbon::now()->addMinutes(10));
                    dispatch($emailJob);
                    //                    Mail::to($user->email)->send(new SendMail($data));
//                    if (env('MAIL_HOST', false) == 'smtp.mailtrap.io') {
//                        sleep(1); //use usleep(500000) for half a second or less
//                    }
                }
            }

            return back()->with('success', 'Thêm bài tập thành công');
        }

        return back()->with('error', 'Thêm bài tập không thành công');
    }

    /**
     * @param Request $request
     * @param int $class_id
     * @param int $post_id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function listAnswerStudent(Request $request, int $class_id, int $post_id)
    {
        $students = AnswerModel::where('post_id', $post_id)->get();

        $listStudentHandIn = AnswerModel::where([
            ['post_id', $post_id],
            ['status', 2],
        ])->get()->count();

        $listStudentHandInScore = AnswerModel::where([
            ['post_id', $post_id],
            ['score', '!=', null]
        ])->get()->count();

        $breadcrumbs = [
            $this->breadcrumb1,
            $this->breadcrumb2,
            new BreadcrumbUtil('List student answer')
        ];

        $dataView = [
            'breadcrumbs' => $breadcrumbs,
            'title' => 'Danh sách sinh viên',
            'students' => $students,
            'listStudentHandIn' => $listStudentHandIn,
            'listStudentHandInScore' => $listStudentHandInScore
        ];

        return view('admin.classes.list-student-answer', $dataView);
    }

}
