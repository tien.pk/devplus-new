<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use App\Models\CourseModel;
use App\Util\BreadcrumbUtil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CourseController extends Controller
{
    public $breadcrumb1;
    public $breadcrumb2;

    public function __construct()
    {
        $this->breadcrumb1 = new BreadcrumbUtil('Dashboard', 'Dashboard');
        $this->breadcrumb2 = new BreadcrumbUtil('Courses', 'ListCourse');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\View
     */
    public function listCourse(Request $request)
    {
        $breadcrumbs = [
            $this->breadcrumb1,
            new BreadcrumbUtil('Courses')
        ];

        $dataView = [
            'title' => 'Courses',
            'courses' => CourseModel::all(),
            'breadcrumbs' => $breadcrumbs
        ];

        return view('admin.courses.index', $dataView);
    }

    public function showAddCourse(Request $request)
    {
        $breadcrumbs = [
            $this->breadcrumb1,
            $this->breadcrumb2,
            new BreadcrumbUtil('Add')
        ];

        $dataView = [
            'breadcrumbs' => $breadcrumbs,
            'title' => 'Thêm khóa học'
        ];

        return view('admin.courses.add', $dataView);
    }

    public function procAddCourse(Request $request)
    {
        Validator::validate($request->all(), [
            'title' => 'required'
        ], [
            'title.required' => 'Tên khóa học bắt buộc phải nhập'
        ]);

        $course = new CourseModel();
        $course->title = $request->get('title');

        $flag = $course->save();
        if ($flag) {
            return redirect(route('ListCourse'))->with('success', 'Thêm khóa học thành công.');
        }

        return redirect(route('ListCourse'))->with('error', 'Thêm khóa học không thành công.');
    }

    public function deleteCourse(Request $request)
    {
        $id = $request->get('id');

        $course = CourseModel::where('id', $id)->first();
        if ($course == null) {
            return response()->json(['message' => 'khóa học không tồn tại']);
        }

        $flag = $course->delete();
        if ($flag) {
            return response()->json(['message' => 'Xóa thành công']);
        }

        return response()->json(['message' => 'Xóa không thành công']);
    }

}
