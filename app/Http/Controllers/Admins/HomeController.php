<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use App\Models\ClassModel;
use App\Models\UserModel;
use App\Util\BreadcrumbUtil;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public $breadcrumb1;

    public function __construct(){
        $this->breadcrumb1 =new BreadcrumbUtil('Dashboard');
    }

    public function index(Request $request){
        $breadcrumbs = [
            $this->breadcrumb1
        ];

        $students = UserModel::where('role', 4)->get()->count();
        $teachers = UserModel::where('role', 3)->get()->count();
        $classes = ClassModel::all()->count();
        $user = auth()->user();

        $data_view = [
            'title' => 'Dashboard',
            'breadcrumbs' => $breadcrumbs,
            'students' => $students,
            'teachers' => $teachers,
            'classes' => $classes,
            'user' => $user
        ];

        return view('admin.layouts.dashboard', $data_view);
    }
}
