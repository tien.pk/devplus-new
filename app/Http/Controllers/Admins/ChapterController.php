<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use App\Models\ChapterModel;
use App\Util\BreadcrumbUtil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ChapterController extends Controller
{
    public $breadcrumb1;
    public $breadcrumb2;

    public function __construct()
    {
        $this->breadcrumb1 = new BreadcrumbUtil('Dashboard', 'Dashboard');
        $this->breadcrumb2 = new BreadcrumbUtil('Courses', 'ListCourse');
    }

    public function listChapter(Request $request, int $id)
    {
        $breadcrumbs = [
            $this->breadcrumb1,
            new BreadcrumbUtil('Courses', 'ListCourse'),
            new BreadcrumbUtil('Chapters'),
        ];

        $dataView = [
            'title' => 'Chapters',
            'chapters' => ChapterModel::where('course_id', $id)->get(),
            'breadcrumbs' => $breadcrumbs,
            'id' => $id
        ];

        return view('admin.chapters.index', $dataView);
    }

    public function showAddChapter(Request $request, int $id)
    {
        $breadcrumbs = [
            $this->breadcrumb1,
            $this->breadcrumb2,
            new BreadcrumbUtil('Courses', 'ListCourse'),
            new BreadcrumbUtil('Add')
        ];

        $dataView = [
            'breadcrumbs' => $breadcrumbs,
            'title' => 'Thêm chương',
            'id' => $id
        ];

        return view('admin.chapters.add', $dataView);
    }

    public function procAddChapter(Request $request, $id)
    {
        Validator::validate($request->all(), [
            'title' => 'required'
        ], [
            'title.required' => 'Tên khóa học bắt buộc phải nhập'
        ]);

        $chapter = new ChapterModel();
        $chapter->title = $request->get('title');
        $chapter->course_id = $id;

        $flag = $chapter->save();
        if ($flag) {
            return redirect(route('ListChapter', $id))->with('success', 'Thêm chương thành công.');
        }

        return redirect(route('ListChapter', $id))->with('error', 'Thêm chương không thành công.');
    }

    public function deleteChapter(Request $request)
    {
        $id = $request->get('id');

        $chapter = ChapterModel::where('id', $id)->first();
        if ($chapter == null) {
            return response()->json(['message' => 'Chương này không tồn tại']);
        }

        $flag = $chapter->delete();
        if ($flag) {
            return response()->json(['message' => 'Xóa thành công']);
        }

        return response()->json(['message' => 'Xóa không thành công']);
    }
}
