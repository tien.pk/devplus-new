<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use App\Jobs\SendEmail;
use App\Models\AnswerModel;
use App\Models\ChapterModel;
use App\Models\ClassDetailModel;
use App\Models\ClassModel;
use App\Models\CourseModel;
use App\Models\PostModel;
use App\Models\UserModel;
use App\Util\BreadcrumbUtil;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ClassController extends Controller
{
    public $breadcrumb1;
    public $breadcrumb2;

    public function __construct()
    {
        $this->breadcrumb1 = new BreadcrumbUtil('Dashboard', 'Dashboard');
        $this->breadcrumb2 = new BreadcrumbUtil('Classes', 'ListClass');
    }

    // Class

    /**
     * @param Request $request
     * @return Illuminate\Contracts\View\View
     */
    public function listClass(Request $request)
    {
        $breadcrumbs = [
            $this->breadcrumb1,
            new BreadcrumbUtil('Classes')
        ];

        $dataView = [
            'title' => 'Danh sách lớp',
            'classes' => ClassModel::all(),
            'breadcrumbs' => $breadcrumbs
        ];

        return view('admin.classes.index', $dataView);
    }

    /**
     * @param Request $request
     * @return Illuminate\Contracts\View\View
     */
    public function showAddClass(Request $request)
    {
        $breadcrumbs = [
            $this->breadcrumb1,
            $this->breadcrumb2,
            new BreadcrumbUtil('Add')
        ];

        $dataView = [
            'breadcrumbs' => $breadcrumbs,
            'title' => 'Thêm lớp',
            'teachers' => UserModel::where('role', 3)->get(),
            'courses' => CourseModel::all()
        ];

        return view('admin.classes.add', $dataView);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Routing\Redirector
     */
    public function procAddClass(Request $request)
    {
        Validator::validate($request->all(), [
            'title' => 'required',
            'course_id' => 'required',
            'teacher_id' => 'required'
        ], [
            'title.required' => 'Tên lớp bắt buộc phải nhập',
            'course_id.required' => 'Chọn khóa học cho lớp',
            'teacher_id.required' => 'Chọn giáo viên cho lớp'
        ]);

        $class = new ClassModel();
        $class->title = $request->get('title');
        $class->user_id = $request->get('teacher_id');
        $class->course_id = $request->get('course_id');

        if ($request->hasFile('avatar')) {
            $file = $request->file('avatar');

            if ($file->isValid()) {
                $fileName = time() . '-' . $file->getClientOriginalName();
                $pathUpload = public_path() . DIRECTORY_SEPARATOR . 'uploads';
                $file->move($pathUpload, $fileName);
                $class->avatar = $fileName;
            }
        }

        $flag = $class->save();
        if ($flag) {
            return redirect(route('ListClass'))->with('success', 'Thêm lớp thành công.');
        }

        return redirect(route('ListClass'))->with('error', 'Thêm lớp khoong thành công.');


    }

    public function showEditClass(Request $request, $id)
    {
        $class = ClassModel::find($id);

        $breadcrumbs = [
            $this->breadcrumb1,
            $this->breadcrumb2,
            new BreadcrumbUtil('Edit')
        ];

        $dataView = [
            'breadcrumbs' => $breadcrumbs,
            'title' => 'Cập nhật lớp',
            'class' => $class,
            'teachers' => UserModel::where('role', 3)->get(),
            'courses' => CourseModel::all()
        ];

        return view('admin.classes.edit-class', $dataView);
    }

    public function procEditClass(Request $request, $id) {
        $class = ClassModel::find($id);
        $class->title = $request->get('title', '');
        $class->course_id = $request->get('course_id');
        $class->user_id = $request->get('teacher_id');

        $flag = $class->save();
        if($flag) {
            return redirect(route('ListClass'))->with('success', 'Cập nhật lớp thành công.');
        }

        return redirect(route('ListClass'))->with('error', 'Cập nhật lớp không thành công.');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteClass(Request $request)
    {
        $id = $request->get('id');

        $class = ClassModel::where('id', $id)->first();
        if ($class == null) {
            return response()->json(['message' => 'Lớp học không tồn tại']);
        }

        $flag = $class->delete();
        if ($flag) {
            return response()->json(['message' => 'Xóa thành công']);
        }

        return response()->json(['message' => 'Xóa không thành công']);
    }

    //Student

    /**
     * @param Request $request
     * @param int $class_id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function listStudent(Request $request, int $class_id)
    {
        $breadcrumbs = [
            $this->breadcrumb1,
            $this->breadcrumb2,
            new BreadcrumbUtil('List student')
        ];

        $student_id = ClassDetailModel::where('class_id', $class_id)->get('id');
        $students = [];

        foreach ($student_id as $id) {
            $arr[] = UserModel::find($id->id);
        }

        $dataView = [
            'breadcrumbs' => $breadcrumbs,
            'title' => 'Danh sách học viên',
            'students' => $arr
        ];

        return view('admin.classes.list-student', $dataView);
    }

    public function statistic($id, $exid){
        // Nộp bài
        $notAnswers = AnswerModel::where('post_id', $exid)->where('status', 1)->get()->count();
        $answers = AnswerModel::where('post_id', $exid)->where('status', 2)->where('add_time', '=' , NULL)->get()->count();
        $lateAnswers = AnswerModel::where('post_id', $exid)->where('status', 2)->where('add_time', '!=' , NULL)->get()->count();

        // Chấm điểm
        $listAnswerHandIn = AnswerModel::where([
            ['post_id', $exid],
            ['status', 2]
        ])->get()->count();

        $listStudentHandInScore = AnswerModel::where([
            ['post_id', $exid],
            ['status', 2],
            ['score', '!=', null]
        ])->get()->count();

        $breadcrumbs = [
            $this->breadcrumb1,
            $this->breadcrumb2,
            new BreadcrumbUtil('Statistic')
        ];

        $dataView = [
            'breadcrumbs' => $breadcrumbs,
            'title' => 'Thống kê',
            'answers' => $answers,
            'notAnswers' => $notAnswers,
            'lateAnswers' => $lateAnswers,
            'listAnswerHandIn' => $listAnswerHandIn,
            'listStudentHandInScore' => $listStudentHandInScore,
        ];

        return view('admin.classes.statistic', $dataView);
    }
}
