<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use App\Imports\UsersImport;
use App\Jobs\SendEmail;
use App\Models\UserModel;
use App\Util\BreadcrumbUtil;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;

class StudentController extends Controller
{
    public $breadcrumb1;
    public $breadcrumb2;

    public function __construct()
    {
        $this->breadcrumb1 = new BreadcrumbUtil('Dashboard', 'Dashboard');
        $this->breadcrumb2 = new BreadcrumbUtil('Students', 'ListStudent');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\View
     */
    public function listStudent(Request $request)
    {
        $breadcrumbs = [
            $this->breadcrumb1,
            new BreadcrumbUtil('Students')
        ];

        $dataView = [
            'title' => 'Students',
            'students' => UserModel::where('role', 4)->get(),
            'breadcrumbs' => $breadcrumbs
        ];

        return view('admin.students.index', $dataView);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\View
     */
    public function showAddStudent(Request $request)
    {
        $breadcrumbs = [
            $this->breadcrumb1,
            $this->breadcrumb2,
            new BreadcrumbUtil('Add')
        ];

        $dataView = [
            'breadcrumbs' => $breadcrumbs,
            'title' => 'Add student'
        ];

        return view('admin.students.add', $dataView);
    }

    public function procAddStudent(Request $request)
    {
        Validator::validate($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users'
        ], [
            'name.required' => 'Họ tên bắt buộc phải nhập',
            'email.required' => 'Email bắt buộc phải nhập',
            'email.email' => 'Email không đúng định dạng',
            'email.unique' => 'Email đã tồn tại trong hệ thống'
        ]);

        $student = new UserModel();
        $student->name = $request->get('name', '');
        $student->email = $request->get('email', '');
        $student->birthday = $request->get('birthday', '');
        $password = Str::random(16);
        $student->password = Hash::make($password);
        $student->role = 4;

        if ($request->hasFile('avatar')) {
            $file = $request->file('avatar');

            if ($file->isValid()) {
                $fileName = time() . '-' . $file->getClientOriginalName();
                $pathUpload = public_path() . DIRECTORY_SEPARATOR . 'uploads';
                $file->move($pathUpload, $fileName);
                $student->avatar = $fileName;
            }
        }

        $flag = $student->save();
        $student->assignRole('student');
//        $student->assignRole('student');
        if ($flag) {
            $data = [
                'title' => 'Trung tâm lập trình Devplus',
                'message' => 'Chào bạn ' . $student->name . '. Đây là mật khẩu truy cập hệ thống Deplus: ' . $password,
                'url' => route('Home'),
                'button' => 'Truy cập hệ thống'
            ];

            $emailJob = (new SendEmail($data, $student->email))->delay(Carbon::now()->addSeconds(15));
            dispatch($emailJob);

//            Mail::to('phunkhactien2001@gmail.com')->send(new SendMail($data));

        }

        return redirect(route('ListStudent'))->with('success', 'Thêm học viên thành công.');
    }

    public function showStudent($id)
    {
        $student = UserModel::find($id);

        if ($student == null) {
            return back()->with('error', 'Học viên không tồn tại');
        }

        $breadcrumbs = [
            $this->breadcrumb1,
            $this->breadcrumb2,
            new BreadcrumbUtil('Show')
        ];

        $dataView = [
            'breadcrumbs' => $breadcrumbs,
            'student' => $student,
            'title' => 'Thông tin học viên'
        ];

        return view('admin.students.show', $dataView);
    }

    public function showEditStudent(Request $request, $id)
    {

        $student = UserModel::find($id);

        if ($student == null) {
            return back()->with('error', 'Học viên không tồn tại');
        }

        $breadcrumbs = [
            $this->breadcrumb1,
            $this->breadcrumb2,
            new BreadcrumbUtil('Edit')
        ];

        $dataView = [
            'breadcrumbs' => $breadcrumbs,
            'student' => $student,
            'title' => 'Cập nhật học viên'
        ];

        return view('admin.students.edit', $dataView);
    }

    public function procEditStudent(Request $request, int $id)
    {
        Validator::validate($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,' . $id
        ], [
            'name.required' => 'Họ tên bắt buộc phải nhập',
            'email.required' => 'Email bắt buộc phải nhập',
            'email.email' => 'Email không đúng định dạng',
            'email.unique' => 'Email đã tồn tại trong hệ thống'
        ]);

        $student = UserModel::find($id);

        if ($student == null) {
            return back()->with('error', 'Học viên không tồn tại');
        }

        $student->name = $request->get('name');
        $student->email = $request->get('email');
        $student->birthday = $request->get('birthday');

        if ($request->hasFile('avatar')) {
            $file = $request->file('avatar');

            if ($file->isValid()) {
                $fileName = time() . '-' . $file->getClientOriginalName();
                $pathUpload = public_path() . DIRECTORY_SEPARATOR . 'uploads';
                $file->move($pathUpload, $fileName);
                $student->avatar = $fileName;
            }
        }

        $flag = $student->save();

        if ($flag) {
            return back()->with('success', 'Cập nhật học viên thành công');
        } else {
            return back()->with('error', 'Cập nhật học viên thất bại');
        }
    }

    public function deleteStudent(Request $request)
    {
        $id = $request->get('id');

        $user = UserModel::find($id);
        if ($user == null) {
            return response()->json(['mesage' => 'Học viên không tồn tại']);
        }

        $flag = $user->delete();
        if ($flag) {
            return response()->json(['message' => 'Xóa học viên thành công']);
        }

        return response()->json(['mesage' => 'Xóa học viên không thành công']);
    }
    // public function importExcell()
    // {
    //     $spreadsheet = new Spreadsheet();
    //     $sheet = $spreadsheet->getActiveSheet();
    // }

    public function importExcel(Request $request)
    {
        if ($request->hasFile('file')) {
            $file = $request->file('file');

            Excel::import(new UsersImport, $file);
            return redirect(route('ListStudent'))->with('success', 'Thêm học viên thành công.');
        }
    }
}
