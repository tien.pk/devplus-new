<?php

namespace App\Http\Controllers\Admins;

use App\Commons\RoleConstant;
use App\Http\Controllers\Controller;
use App\Models\ChapterModel;
use App\Models\ClassModel;
use App\Models\CourseModel;
use App\Models\PostModel;
use App\Models\UserModel;
use App\Util\BreadcrumbUtil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class TeacherController extends Controller
{
    public $breadcrumb1;
    public $breadcrumb2;

    public function __construct()
    {
        $this->breadcrumb1 = new BreadcrumbUtil('Dashboard', 'Dashboard');
        $this->breadcrumb2 = new BreadcrumbUtil('Teachers', 'ListTeacher');
    }

    public function listTeacher(Request $request)
    {
        $breadcrumbs = [
            $this->breadcrumb1,
            new BreadcrumbUtil('Teachers')
        ];

        $dataView = [
            'title' => 'Teachers',
            'teachers' => UserModel::where('role', 3)->get(),
            'breadcrumbs' => $breadcrumbs
        ];

        return view('admin.teachers.index', $dataView);
    }

    public function showAddTeacher(Request $request)
    {
        $breadcrumbs = [
            $this->breadcrumb1,
            $this->breadcrumb2,
            new BreadcrumbUtil('Add')
        ];

        $dataView = [
            'breadcrumbs' => $breadcrumbs,
            'title' => 'Add teacher'
        ];

        return view('admin.teachers.add', $dataView);
    }

    public function procAddTeacher(Request $request)
    {
        Validator::validate($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users'
        ], [
            'name.required' => 'Họ tên bắt buộc phải nhập',
            'email.required' => 'Email bắt buộc phải nhập',
            'email.email' => 'Email không đúng định dạng',
            'email.unique' => 'Email đã tồn tại trong hệ thống'
        ]);

        $teacher = new UserModel();
        $teacher->name = $request->get('name', '');
        $teacher->email = $request->get('email', '');
        $teacher->birthday = $request->get('birthday', '');
        $password = Str::random(16);
        $teacher->password = Hash::make('12345678');
        $teacher->role = 3;

        if ($request->hasFile('avatar')) {
            $file = $request->file('avatar');

            if ($file->isValid()) {
                $fileName = time() . '-' . $file->getClientOriginalName();
                $pathUpload = public_path() . DIRECTORY_SEPARATOR . 'uploads';
                $file->move($pathUpload, $fileName);
                $teacher->avatar = $fileName;
            }
        }

        $teacher->save();
//        $teacher->assignRole('teacher');

        // Add role cho teacher
//        $role = Role::find(1);
//        $permission = Permission::find(2);
//        $role->givePermissionTo($permission);
        $teacher->assignRole(RoleConstant::TEACHER_TEXT);
        $teacher->givePermissionTo('teacher');

        return redirect(route('ListTeacher'))->with('success', 'Thêm giáo viên thành công.');
    }

    public function showTeacher($id)
    {
        $teacher = UserModel::find($id);

        if ($teacher == null) {
            return back()->with('error', 'Giáo viên không tồn tại');
        }

        $breadcrumbs = [
            $this->breadcrumb1,
            $this->breadcrumb2,
            new BreadcrumbUtil('Show')
        ];

        $dataView = [
            'breadcrumbs' => $breadcrumbs,
            'teachers' => $teacher,
            'title' => 'Thông tin giáo viên'
        ];

        return view('admin.teachers.show', $dataView);
    }

    public function showEditTeacher(Request $request, $id)
    {

        $teacher = UserModel::find($id);

        if ($teacher == null) {
            return back()->with('error', 'Giáo viên không tồn tại');
        }

        $breadcrumbs = [
            $this->breadcrumb1,
            $this->breadcrumb2,
            new BreadcrumbUtil('Edit')
        ];

        $dataView = [
            'breadcrumbs' => $breadcrumbs,
            'teacher' => $teacher,
            'title' => 'Cập nhật giáo viên'
        ];

        return view('admin.teachers.edit', $dataView);
    }

    public function procEditTeacher(Request $request, int $id)
    {
        Validator::validate($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,' . $id
        ], [
            'name.required' => 'Họ tên bắt buộc phải nhập',
            'email.required' => 'Email bắt buộc phải nhập',
            'email.email' => 'Email không đúng định dạng',
            'email.unique' => 'Email đã tồn tại trong hệ thống'
        ]);

        $teacher = UserModel::find($id);

        if ($teacher == null) {
            return back()->with('error', 'Giáo viên không tồn tại');
        }

        $teacher->name = $request->get('name');
        $teacher->email = $request->get('email');
        $teacher->birthday = $request->get('birthday');

        if ($request->hasFile('avatar')) {
            $file = $request->file('avatar');

            if ($file->isValid()) {
                $fileName = time() . '-' . $file->getClientOriginalName();
                $pathUpload = public_path() . DIRECTORY_SEPARATOR . 'uploads';
                $file->move($pathUpload, $fileName);
                $teacher->avatar = $fileName;
            }
        }

        $flag = $teacher->save();

        if ($flag) {
            return back()->with('success', 'Cập nhật giáo viên thành công');
        } else {
            return back()->with('error', 'Cập nhật giáo viên thất bại');
        }
    }

    public function deleteTeacher(Request $request)
    {
        $id = $request->get('id');

        $user = UserModel::find($id);
        if ($user == null) {
            return response()->json(['mesage' => 'Giáo viên này không tồn tại']);
        }

        $flag = $user->delete();
        if ($flag) {
            return response()->json(['message' => 'Xóa giáo viên thành công']);
        }

        return response()->json(['mesage' => 'Xóa giáo viên không thành công']);
    }

    public function statisticTeacher($id){
        $breadcrumbs = [
            $this->breadcrumb1,
            $this->breadcrumb2,
            new BreadcrumbUtil('Statistic')
        ];

        $courses = [];
        $classes = ClassModel::where('user_id', $id)->distinct()->get('course_id');
        
        if(!empty($classes)){
            foreach($classes as $class){
                $courses[] = CourseModel::find($class->course_id);
                if(!empty($courses)){
                    foreach($courses as $course){
                        $clas = ClassModel::where('user_id', $id)->where('course_id', $course->id)->get();
                        if(!empty($clas)){
                            $course->countPosts = 0;
                            foreach($clas as $value){
                                $course->countPosts += PostModel::where('user_id', $id)->where('class_id', $value->id)->get()->count();
                            }
                            $course->countPosts = number_format($course->countPosts / $classes->count(), 2);
                        }
                        $chapters = ChapterModel::where('course_id', $course->id)->get();
                        if(!empty($chapters)){
                            $course->quantity = 0;
                            foreach($chapters as $chapter){
                                $course->quantity += $chapter->quantity;
                            }
                        }
                    }
                }
            }
        }

        $dataView = [
            'breadcrumbs' => $breadcrumbs,
            'courses' => $courses,
            'teacher_id' => $id,
            'title' => 'Thống kê'
        ];

        return view('admin.teachers.statistic', $dataView);
    }
    public function statisticTeacherChapter($user_id, $course_id){
        $breadcrumbs = [
            $this->breadcrumb1,
            $this->breadcrumb2,
            new BreadcrumbUtil('Statistic Chapter')
        ];

        $chapters = ChapterModel::where('course_id', $course_id)->get();

        if(!empty($chapters)){
            foreach($chapters as $chapter){
                $chapter->countPosts = PostModel::where('user_id', $user_id)->where('chapter_id', $chapter->id)->get()->count();
            }
        }
        $dataView = [
            'breadcrumbs' => $breadcrumbs,
            'chapters' => $chapters,
            'title' => 'Thống kê học phần'
        ];

        return view('admin.teachers.statistic-chapter', $dataView);
    }
}
