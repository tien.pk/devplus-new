<?php

namespace App\Commons;

class RoleConstant {
    const ADMIN = 1;
    const TEACHER = 3;
    const STUDENT = 4;

    const ADMIN_TEXT = 'admin';
    const TEACHER_TEXT = 'teacher';
    const STUDENT_TEXT = 'student';

}
