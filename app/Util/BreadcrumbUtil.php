<?php

namespace App\Util;

class BreadcrumbUtil
{
    public string $text;
    public ?string $route_name;

    public function __construct(string $text, ?string $route_name = null){
        $this->text = $text;
        $this->route_name = $route_name;
    }


}
